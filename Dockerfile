FROM nginx

COPY index.html /usr/share/nginx/html
COPY keybase.txt /usr/share/nginx/html

COPY css /usr/share/nginx/html/css
COPY img /usr/share/nginx/html/img
COPY content /usr/share/nginx/html/content
COPY .well-known /usr/share/nginx/html/.well-known
